
#' Construct a set
#'
#' A lglset object is a bitset like implementation of a set.
#' It consists of two elements \code{x} and \code{universe} such that \code{x}
#' is a logical vector indicating which element of \code{universe} belongs to the set.
#'
#' \code{x} and \code{universe} should have the same size.
#'
#' @param x A logical vector indicating which element belongs to the set
#' @param universe A vector or list of elements describing the elements allowed to belong to the resulting set
#' @return A lglset object
#' @export
#' @examples
#'   universe <- c("a", "b", "c")
#'
#'   # x contains "a" and "b", and its set of authorized values is "a", "b" and "c"
#'   x <- lglset(c(TRUE, TRUE, FALSE), universe)
#'
#'   # Or with a factory
#'   factory <- lglset_factory(universe)
#'   y <- factory(c("a", "b"))
#'
#'   identical(x, y)
lglset <- function(x, universe) {
  structure(
    list(bits = x, universe = universe),
    class = "lglset"
  )
}

#' @export
#' @describeIn lglset Factory for object of type lglset
lglset_factory <- function(universe) {
  function(x) {
    lglset(universe %in% x, universe)
  }
}

#' @export
#' @rdname lglset 
length.lglset <- function(x) {
  sum(x$bits)
}

#' Elements in a set object
#'
#' @param x A lglset object
#' @return The elements belonging to \code{x}.
#' @export
elements <- function(x) {
  UseMethod("elements", x)
}

#' @describeIn elements Elements in a lglset
#' @export
elements.lglset <- function(x) {
  x$universe[x$bits]
}

#' Universe of a set object
#'
#' @param x A lglset object
#' @return The range of elements authorized to belong to \code{x]}
#' @export
universe <- function(x) {
  UseMethod("universe", x)
}

#' @describeIn universe Universe in a lglset
#' @export
universe.lglset <- function(x) {
  x$universe
}

#' Type conversion
#'
#' @param x A lglset object
#' @export
#' @rdname type-lglset
is.lglset <- function(x) {
  identical(class(x), "lglset")
}

#' @param ... Unused
#' @export
#' @rdname type-lglset
as.list.lglset <- function(x, ...) {
  elements(x)
}
